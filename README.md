This script helps you install Arch Linux faster and easier.

<b>USAGE INSTRUCTIONS</b>

- Boot up Arch Linux ISO
- Set keyboard layout (optional):

<i>loadkeys your_kb_layout</i>

- Install wget package using pacman (optional):

<i>pacman -S wget</i>

- Download and execute this script using short url and wget (optional):

<i>wget is.gd/ALISsh && sh ALISsh</i>

- **OR** just use _curl_ to download and then execute this script:

<i>curl is.gd/ALISsh -Lo ALIS && sh ALIS</i>

- Follow the instructions given by script
