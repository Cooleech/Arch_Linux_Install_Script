#!/bin/bash
#####################################
# What  : Arch_Linux_Install_Script #
# Which : version 2.30              #
# When  : 10.03.2024.               #
# Who	: Cooleech                  #
# Where : GPLv2                     #
# Write : cooleechATgmail.com       #
#####################################
#==============================================================================#
setfont Lat2-Terminus16 # Postavljam font (podržava sva naša slova)
clear
echo -e "\e[1;34mOdaberite jezik instalera\e[0m | \e[1;34mPick installer's language\e[0m:\n\n \e[36mh\e[0m = \e[36mHrvatski\e[0m <= default\n\n \e[36me\e[0m = \e[36mEnglish\e[0m\n"
read Language
Language="${Language,,}"
case "$Language" in
e*)
Enter="Enter"
YourFullName="your full name and surname"
Warning="WARNING"
UsernameHasDot="Your username cannot contain any dots!"
NoUsername="You didn't provide any username"
EnterPass="Enter password for user"
NoDisplaying="( will not display entered )"
ReEnterPass="Reenter password for user"
AvailableDisks="Available disks"
DiskToPart="which disk you want to partition"
Without="WITHOUT"
And="and"
Numbers="numbers"
Eg="eg"
ToSkipPart="to skip partitioning just close cfdisk"
Error="ERROR"
YouPicked="You picked"
TryAgain="Let's try again"
ToContinue="To continue press Enter"
ToGoBack="to go back, pick letter B"
DiskAccessError="which cfdisk cannot access.\nChoose a disk again"
ViewPartitions="View of disk partitions"
Number="NUMBER"
OfDiskPartition="of disk partition for"
PartitionError="partition *MUST* be selected"
CheckInternet="Checking internet connection"
ConnectUsingWiFi="Would you like to use wifi connection"
Yn="Y/n"
ToConnectUse="To connect to Wi-Fi network, use command ( in tty2 ):"
NoInternet="There's no internet connection! :("
PleaseCheck="Please check the cable or your net device settings.\nThis installation NEEDS internet conection to finish successfuly"
ContinueOrCancel="Press Enter to continue\nCtrl + C to cancel instalation"
PassEmpty="Password cannot be blank"
PassMismatch="Passwords do not match"
Welcome="Welcome to simplified installation of"
ArchLinux="Arch Linux"
WhyThisScript="This script is here to simplify $ArchLinux install process"
YourRisk="YOU ARE USING IT AT YOUR OWN RISK"
InfoGathering="First, we'll pick some information. So, let's go"
WhichKeyLayout="Which keyboard layout would you like to use"
Croatian="Croatian"
English="English"
Other="Other"
Wrong="Wrong"
KeyboardLayout="keyboard layout"
YouWantToUse="you wish to use after installation"
LayoutEg="for croatian layout"
LayoutAfterInstall="After installation, keyboard layout will be set to"
ToSkip="to skip, just press Enter"
ToFormat="Would you like to format"
Partition="partition"
YesToExt="yes, to ext4"
AllDataFrom="All data from"
WillBeErased="partition will be erased"
NoSepHomePart="no separate home partition"
Yes="yes"
No="no"
NoSwapPart="swap partition not selected"
PartNoTaken="Partition is already taken! Pick other one."
EnterHostName="Enter hostname ( no spaces, just Enter for"
EnterDE="Enter a single letter beside the DE you want to install"
IllPickLater="None. I'll install DE or WM later"
AutoLoginAs="Would you like to use autologin for"
Navigation="Navigation\: Use PageUp or PageDown to scroll, Q to quit"
AtLogin="at login"
NumLockOn="Would you like to have Num Lock turned on $AtLogin ( doesn't work in Gnome )"
WillBeOn="will be turned on"
WillBeOff="will be turned off"
NoDEorWMinstall="You decided to skip DE ( or WM ) install"
yN="y/N"
Overview="Important settings overview"
UserNameIs="User name:\t"
HostNameIs="Host name"
DiskPart="Disk partitions"
FormPart="Formating partitions"
YesTo="yes, to"
YesIfSel="yes ( if selected"
KeyLayout="Keyboard"
AllInfoIneed="That's it. I've got enough to proceed with the installation.\nJust sit back and relax until installation finishes"
InstallEnd="INSTALLATION ENDED"
EnjoyWith="Enjoy with your new"
RebootIn="Rebooting in 5 seconds..."
;;
*)
Enter="Upišite"
YourFullName="svoje puno ime i prezime"
Warning="UPOZORENJE"
UsernameHasDot="Korisničko ime ne može sadržavati točku/e!"
NoUsername="Niste upisali korisničko ime"
EnterPass="Upišite lozinku za korisnika"
NoDisplaying="( neće prikazati unos )"
ReEnterPass="Ponovo upišite lozinku za korisnika"
AvailableDisks="Dostupni diskovi"
DiskToPart="disk koji želite particionirati"
Without="BEZ"
And="i"
Eg="npr"
Numbers="brojki"
ToSkipPart="za preskok particioniranja samo zatvorite cfdisk"
Error="GREŠKA"
YouPicked="Odabrali ste"
TryAgain="Pokušajmo ponovo"
ToContinue="Za nastavak stisnite Enter"
ToGoBack="za povratak natrag, odaberite slovo N"
DiskAccessError="kojemu cfdisk ne može pristupiti.\nPonovite odabir diska"
ViewPartitions="Pregled stanja particija na disku"
Number="BROJ"
OfDiskPartition="particije diska za"
PartitionError="particija *MORA* biti odabrana"
CheckInternet="Provjeravam konekciju na internet"
ConnectUsingWiFi="Želite se spojiti bežično"
Yn="D/n"
ToConnectUse="Da se spojite na Wi-Fi mrežu, koristite naredbu ( u tty2 ):"
NoInternet="Nema dostupne internet veze! :("
PleaseCheck="Provjerite kabel ili postavke mrežnog uređaja.\nDa bi se uspješno obavila, ova instalacija TREBA vezu s internetom"
ContinueOrCancel="Pritisnite Enter za nastavak ili Ctrl + C za prekid instalacije"
PassEmpty="Lozinka ne može biti prazna"
PassMismatch="Lozinke se ne podudaraju"
Welcome="Dobrodošli u pojednostavljenu instalaciju"
ArchLinux="Arch Linuxa"
WhyThisScript="Ova skripta je tu da vam olakša instalaciju $ArchLinux"
YourRisk="KORISTITE JE NA VLASTITU ODGOVORNOST"
InfoGathering="Za početak, prikupit ćemo neke informacije. Pa, krenimo"
WhichKeyLayout="Koji raspored tipkovnice ( keyboard layout ) želite koristiti"
Croatian="Hrvatski"
English="Američki"
Other="Ostali"
Wrong="Neispravan"
KeyboardLayout="raspored tipkovnice"
YouWantToUse="koji želite koristiti nakon instalacije"
LayoutEg="za hrvatski raspored"
LayoutAfterInstall="Raspored tipkovnice nakon instalacije bit će postavljen na"
ToSkip="za preskok samo pritisnite Enter"
ToFormat="Želite li formatirati"
Partition="particiju"
YesToExt="da, u ext4"
AllDataFrom=" Svi podaci na"
WillBeErased="particija bit će obrisana"
NoSepHomePart="nema zasebne home particije"
Yes="da"
No="ne"
NoSwapPart="niste odabrali swap particiju"
PartNoTaken="Particija je već zauzeta! Odaberite drugu."
EnterHostName="Upišite ime hosta ( bez razmaka, samo Enter za"
EnterDE="Upišite slovo pored DE-a koji želite instalalirati"
IllPickLater="Nijedan. Kasnije ću instalirati DE ili WM"
AutoLoginAs="Želite li biti automatski ulogirani kao"
Navigation="Navigacija\: koristite PageUp ili PageDown za pomicanje, Q za izlaz"
AtLogin="pri logiranju"
NumLockOn="Želite li imati uključen Num Lock $AtLogin u sustav ( ne radi na Gnomeu )"
WillBeOn="bit će uključen"
WillBeOff="neće biti uključen"
NoDEorWMinstall="Odlučili ste preskočiti instalaciju DE-a ( ili WM-a )"
yN="d/N"
Overview="Pregled važnijih postavki"
UserNameIs="Korisničko ime:"
HostNameIs="Ime hosta"
DiskPart="Particije diska"
FormPart="Formatiranje particija"
YesTo="da, u"
YesIfSel="da ( ako je odabran"
KeyLayout="Tipkovnica"
AllInfoIneed="To bi bilo to. Imam dovoljno informacija za nastavak instalacije.\nSamo sjednite i opustite se dok se instalacija ne obavi do kraja"
InstallEnd="KRAJ INSTALACIJE"
EnjoyWith="Sretno uz novu instalaciju"
RebootIn="Reboot za 5 sekundi..."
;;
esac
#==============================================================================#
function ENTER_KEYBOARD_LAYOUT {
clear
# TODO Dodati prikaz keyboard mapa
echo -e "\e[1;34m$Enter $KeyboardLayout $YouWantToUse\e[0m\n\t( $Eg. \e[1;36mcroat\e[0m $LayoutEg )\n"
read Layout
Layout="${Layout,,}"
localectl list-keymaps | grep -x "$Layout" > /dev/null
if [ $? != 0 ]; then
 echo -e "\n \e[1;31m*** $Error ***\e[0m\n\n $Wrong $KeyboardLayout! \n"
 sleep 2
 ENTER_KEYBOARD_LAYOUT
else
 echo -e "$LayoutAfterInstall \e[1;36m${Layout^^}\e[0m" && sleep 2
fi
}

function USER_NAME {
clear
echo -e "\e[1;34m$Enter $YourFullName:\e[0m"
read FullName
UserName="${FullName%% *}" # Ostavi samo prvu riječ
UserName="${UserName,,}" # Konverzija u lowercase
clear
case "$UserName" in
*.*)
 echo -e "\n \e[1;31m* $Error *\e[0m\n $UsernameHasDot\n"
 CONTINUE_OR_CANCEL
 USER_NAME
;;
"")
 echo -e "\n \e[1;31m* $Error *\e[0m\n $NoUsername...\n"
 CONTINUE_OR_CANCEL
 USER_NAME
;;
esac
}

function ENTER_USER_PASS {
clear
Pass1=""
Pass2=""
echo -e "\e[1;34m$EnterPass \e[1;36m$UserName\e[1;34m $NoDisplaying:\e[0m"
stty -echo
read Pass1
stty echo
if [ "$Pass1" = "" ]; then
 PASSWORD_EMPTY
 ENTER_USER_PASS
else
 CONFIRM_USER_PASS
fi
}

function CONFIRM_USER_PASS {
echo -e "\e[1;34m$ReEnterPass \e[1;36m$UserName \e[1;34m$NoDisplaying:\e[0m"
stty -echo
read Pass2
stty echo
if [ "$Pass1" != "$Pass2" ]; then
 PASSWORD_MISMATCH
 ENTER_USER_PASS
fi
}

function ENTER_ROOT_PASS {
clear
Pass3=""
Pass4=""
echo -e "\e[1;34m$EnterPass \e[1;31mroot \e[1;34m$NoDisplaying:\e[0m"
stty -echo
read Pass3
stty echo
if [ "$Pass3" = "" ]; then
 PASSWORD_EMPTY
 ENTER_ROOT_PASS
else
 CONFIRM_ROOT_PASS
fi
}

function CONFIRM_ROOT_PASS {
echo -e "\e[1;34m$ReEnterPass \e[1;31mroot \e[1;34m$NoDisplaying:\e[0m"
stty -echo
read Pass4
stty echo
if [ "$Pass3" != "$Pass4" ]; then
 PASSWORD_MISMATCH
 ENTER_ROOT_PASS
fi
}

function PARTITIONING {
clear
echo -e "\e[1;34m$AvailableDisks:\e[0m"
lsblk -o model,name,size,type,tran,mountpoint | grep -v "fd0\|sr0"
echo -e "\n\e[1;34m$Enter $DiskToPart \e[0m\e[35m$Without /dev/ \e[1;34m$And\e[0m \e[35m$Without $Numbers\e[1;34m ( $Eg. \e[36msda\e[1;34m ):\e[0m\n\t( $ToSkipPart )"
read Disk
Disk="${Disk,,}" # Sve u mala slova
Disk="${Disk//'/dev/'/}" # Ukloni /dev/
Disk="${Disk// /}" # Ukloni razmake
if [ "$Disk" = "" ]; then
 clear
 echo -e "\n \e[1;31m* $Error *\e[0m\n\n $YouPicked \"$Disk\" disk! $TryAgain.\n"
 CONTINUE_OR_CANCEL
 PARTITIONING
fi
clear
echo -e "\e[1;34m$AvailableDisks:\e[0m"
lsblk -o model,name,size,type,tran,mountpoint /dev/$Disk
if [ $? != 0 ]; then
 clear
 echo -e "\n \e[1;31m* $Error *\e[0m\n\n $YouPicked \"$Disk\" disk! $TryAgain.\n"
 CONTINUE_OR_CANCEL
 PARTITIONING
fi
echo -e "\n\e[1;34mRučno ili automatsko particioniranje diska \e[1;36m$Disk\e[0m?\n\t( $ToGoBack )\n\n \e[36mR\e[0m = \e[36mRučno particioniranje ( cfdisk )\e[0m <= default\n\n \e[36mA\e[0m = \e[36mAutomatsko particioniranje\e[0m\n"
read DiskPartitioning
case "$DiskPartitioning" in
a*|A*)
 EfiPartSize="500"
 SwapPartSize=`free -g | grep Mem | awk '{ print $2 }'`
 SwapPartSize="$(( SwapPartSize + 1 ))"
 DiskSize=`lsblk -bo size /dev/$Disk | head -n2 | tail -n1`
 DiskSize="$(( DiskSize / 1024 / 1024 / 1024 ))"
 case "$DiskSize" in
 ?)
  echo -e "\n\e[1;31mNažalost, vaš disk ima premalo prostora za instalaciju Arch Linuxa. :(\e[0m\n\nMožete pokušati ručno particioniranje ako imate bar 7 GB prostora na disku."
  read -p ""
  exit
 ;;
 1?)
  RootPartSize="9"
  if [ $(( DiskSize - RootPartSize )) -lt $SwapPartSize ]; then
   SwapPartSize=$(( DiskSize - RootPartSize ))
  fi
 ;;
 2?|3?|4?|5?|6?|7?)
  RootPartSize="$(( DiskSize / 2 ))"
  if [ $(( DiskSize - RootPartSize )) -lt $SwapPartSize ]; then
   SwapPartSize=$(( DiskSize - RootPartSize ))
  fi
 ;;
 *)
  RootPartSize="40"
 ;;
 esac
 if [ $SwapPartSize -gt 8 ]; then
  SwapPartSize="8"
 fi
 HomePartSize="$(( DiskSize - RootPartSize - SwapPartSize ))"
 if [ $HomePartSize -lt 1 ]; then
  clear
  HomePartSize="0"
  echo -e "\e[33mZasebna home particija ne može biti dodana jer disk nema dovoljno slobodnog prostora. :( \e[0m\n\n Disk: $DiskSize GB\t/dev/$Disk\n Swap: $SwapPartSize GB\t/dev/${Disk}1\n Root: $RootPartSize GB\t/dev/${Disk}2\n"
  CONTINUE_OR_CANCEL
 fi
 clear
 # Uređivanje prikaza veličine particija
 if [ $SwapPartSize -gt $RootPartSize ]; then
  SwapSpace="GB     "
  RootSpace="GB"
 elif [ $SwapPartSize -eq $RootPartSize ]; then
  SwapSpace="GB     "
  RootSpace="GB     "
 else
  SwapSpace="GB"
  RootSpace="GB     "
 fi
 if [ $RootPartSize -gt $HomePartSize ]; then
  RootSpace="$RootSpace      "
  HomeSpace=" GB     "
 elif [ $RootPartSize -eq $HomePartSize ]; then
  RootSpace="GB     "
  HomeSpace=" GB     "
 else
  HomeSpace=" GB           "
 fi
 HomePartSize=" HOME: $HomePartSize"
 if [ "$HomePartSize" = " HOME: 0" ]; then
  RootSpace="GB                               "
  HomePartSize=""
  HomeSpace=""
 fi
 echo -e "\n \e[1;31m*** OVO ĆE OBRISATI SVE PODATKE S \e[1;36m/dev/$Disk\e[1;31m DISKA ***\e[0m"
 if [ $DiskSize -gt 2000 ]; then
  PartitionTable="g" # Mora biti GPT
 else
  echo -e "\n\e[1;34mOdaberite particijsku tablicu:\e[0m\n\n \e[36mG\e[0m = \e[36mGPT ( moderni PC-i )\n\n M\e[0m = \e[36mMBR ( PC stariji od 2010. )\e[0m <= default\n"
  read PartitionTable
 fi
 case "$PartitionTable" in
 g*|G*)
  clear
  ls /sys/firmware/efi/efivars &> /dev/null
  if [ $? = 0 ]; then
   case "${Disk:0:4}" in
   "mmcb"|"nvme")
    EfiPart="p1"
    SwapPart="p2"
    RootPart="p3"
    HomePart="p4"
   ;;
   *)
    EfiPart="1"
    SwapPart="2"
    RootPart="3"
    HomePart="4"
   ;;
   esac
   Efi="EFI : $Disk$EfiPart"
   echo -e "\e[1;34mPregled particija na\e[0m \e[36m/dev/$Disk\e[0m \e[1;34mdisku nakon partcioniranja:\e[0m\n\n \e[7;32mEFI: $EfiPartSize MB\e[7;33m SWAP: $SwapPartSize $SwapSpace \e[35m ROOT: $RootPartSize $RootSpace \e[36m$HomePartSize$HomeSpace\e[0m\n"
  else
   case "${Disk:0:4}" in
   "mmcb"|"nvme")
    SwapPart="p1"
    RootPart="p2"
    HomePart="p3"
   ;;
   *)
    SwapPart="1"
    RootPart="2"
    HomePart="3"
   ;;
   esac
   if [ "$HomePartSize" != "0" ]; then
    Home="Home: $Disk$HomePart"
    WillFormatHome="$YesToExt\n\t\e[1;31m( * $Warning * $Disk$HomePart $WillBeErased! )"
   else
    HomePart=""
   fi
   EfiPart=""
   Efi=""
   echo -e "\e[1;34mPregled particija na\e[0m \e[36m/dev/$Disk\e[0m \e[1;34mdisku nakon partcioniranja:\e[0m\n\n \e[7;32mSWAP: $SwapPartSize $SwapSpace \e[35m ROOT: $RootPartSize $RootSpace \e[36m$HomePartSize$HomeSpace\e[0m\n"
  fi
  CONTINUE_OR_CANCEL
  echo " Particioniranje diska kreće za 5..."
  sleep 1
  echo -e " \e[33mParticioniranje diska kreće za 4..."
  sleep 1
  echo -e " \e[1;33mParticioniranje diska kreće za 3..."
  sleep 1
  echo -e " \e[31mParticioniranje diska kreće za 2...\e[0m"
  sleep 1
  echo -e " \e[1;31mParticioniranje diska kreće za 1...\e[0m"
  sleep 1
  if [ "$EfiPart" != "" ]; then
   echo -e "g\nn\n\n\n+500M\nn\n\n\n+${SwapPartSize}G\nn\n\n\n+${RootPartSize}G\nn\n\n\n\nw" | fdisk -W always /dev/$Disk
  else
   echo -e "g\nn\n\n\n+${SwapPartSize}G\nn\n\n\n+${RootPartSize}G\nn\n\n\n\nw" | fdisk -W always /dev/$Disk
  fi
  sleep 1
 ;;
 m*|M*|"")
  clear
  echo -e "\e[1;34mPregled particija na\e[0m \e[36m/dev/$Disk\e[0m \e[1;34mdisku nakon partcioniranja:\e[0m\n\n \e[7;32m SWAP: $SwapPartSize $SwapSpace \e[35m ROOT: $RootPartSize $RootSpace \e[36m$HomePartSize$HomeSpace\e[0m\n"
  CONTINUE_OR_CANCEL
  echo " Particioniranje diska kreće za 5..."
  sleep 1
  echo -e " \e[33mParticioniranje diska kreće za 4..."
  sleep 1
  echo -e " \e[1;33mParticioniranje diska kreće za 3..."
  sleep 1
  echo -e " \e[31mParticioniranje diska kreće za 2...\e[0m"
  sleep 1
  echo -e " \e[1;31mParticioniranje diska kreće za 1...\e[0m"
  sleep 1
  echo -e "o\nn\np\n\n\n+${SwapPartSize}G\nn\np\n\n\n+${RootPartSize}G\nn\np\n\n\n\nw" | fdisk -W always /dev/$Disk
  sleep 1
  case "${Disk:0:4}" in
  "mmcb"|"nvme")
   SwapPart="p1"
   RootPart="p2"
   HomePart="p3"
  ;;
  *)
   SwapPart="1"
   RootPart="2"
   HomePart="3"
  ;;
  esac
 ;;
 esac
 Swap="Swap: $Disk$SwapPart"
 if [ "$HomePartSize" != "0" ]; then
  Home="Home: $Disk$HomePart"
  WillFormatHome="$YesToExt\n\t\e[1;31m( * $Warning * $Disk$HomePart $WillBeErased! )"
 else
  HomePart=""
 fi
;;
*)
 if [ $DiskSize -gt 2000 ]; then
  clear
  echo -e "\e[1;34mDisk je veći od 2 TB, morate napraviti GPT particijsku tablicu ( pitat će vas u nastavku ).\e[0m"
  CONTINUE_OR_CANCEL
 fi
 clear
 cfdisk /dev/$Disk
 if [ $? != 0 ]; then
  clear
  echo -e "\n \e[1;31m* $Error *\e[0m\n\n $YouPicked \"$Disk\" disk $DiskAccessError! \n"
  CONTINUE_OR_CANCEL
  PARTITIONING
 fi
 sleep 1 # Pauza da lsblk uspije uredno pročitat disk
 lsblk /dev/$Disk -o type | grep part
 if [ $? != 0 ]; then
  clear
  echo -e "\n \e[1;31m* $Error *\e[0m\n\n Disk nema niti jednu particiju! \n Morate ponoviti particioniranje.\n"
  CONTINUE_OR_CANCEL
  PARTITIONING
 fi
;;
esac
clear
sleep 1 # Pauza da lsblk uspije uredno pročitat disk
echo -e "\e[1;34m$ViewPartitions \e[1;33m/dev/$Disk\e[0m:\n"
lsblk -o model,name,size,fstype,type,mountpoint /dev/$Disk
if [ "${DiskPartitioning,,}" != "a" ]; then
 fdisk -l /dev/$Disk | grep gpt
 if [ $? = 0 ]; then
  ls /sys/firmware/efi/efivars &> /dev/null
  if [ $? = 0 ]; then
   SEL_EFI_PARTITION
  fi
 fi
 SEL_SWAP_PARTITION
 SEL_ROOT_PARTITION
 SEL_HOME_PARTITION
fi
}

SEL_EFI_PARTITION () {
echo -e "\n\e[1;34m$Enter \e[1;36m$Number \e[1;34m$OfDiskPartition EFI\e[0m\n\t( $Without \e[1;33m/dev/$Disk\e[0m, $ToSkip ):"
read EfiPart
EfiPart="${EfiPart//'/dev/$Disk'/}" # Za svaki slučaj... :)
case "$EfiPart" in
[1-9]|[1-9][0-9]|1[0-2][0-8])
 case "${Disk:0:4}" in
 "mmcb"|"nvme")
  EfiPart="p$EfiPart"
 ;;
 esac
 Efi="EFI : $Disk$EfiPart"
;;
"")
 Efi=""
;;
*)
 clear
 SEL_EFI_PARTITION
;;
esac
}

SEL_SWAP_PARTITION () {
echo -e "\n\e[1;34m$Enter \e[1;36m$Number \e[1;34m$OfDiskPartition swap\e[0m\n\t( $Without \e[1;33m/dev/$Disk\e[0m, $ToSkip ):"
read SwapPart
SwapPart="${SwapPart//'/dev/$Disk'/}" # Za svaki slučaj... :)
case "$SwapPart" in
"")
 Swap="Swap: ništa ( $NoSwapPart )"
;;
[1-9]|[1-9][0-9]|1[0-2][0-8])
 case "${Disk:0:4}" in
 "mmcb"|"nvme")
  SwapPart="p$SwapPart"
 ;;
 esac
 if [ "$EfiPart" != "" ]; then
  if [ $EfiPart = $SwapPart ]; then
   echo -e "\n \e[1;31m* $Error *\e[0m\n\n $PartNoTaken\n"
   CONTINUE_OR_CANCEL
   SEL_SWAP_PARTITION
  fi
 fi
 Swap="Swap: $Disk$SwapPart"
;;
*)
 clear
 SEL_SWAP_PARTITION
;;
esac
}

function SEL_ROOT_PARTITION {
echo -e "\n\e[1;34m$Enter \e[1;36m$Number \e[1;34m$OfDiskPartition / ( root )\e[0m\n\t( $Without \e[1;33m/dev/$Disk\e[0m, $Eg. \e[36m2\e[0m ):"
read RootPart
RootPart="${RootPart//'/dev/$Disk'/}" # Za svaki slučaj... :)
case "$RootPart" in
[1-9]|[1-9][0-9]|1[0-2][0-8])
 case "${Disk:0:4}" in
 "mmcb"|"nvme")
  RootPart="p$RootPart"
 ;;
 esac
 case "$RootPart" in
 ""|"p")
  clear
  echo -e "\n \e[1;31m* $Error *\e[0m\n\n Root $PartitionError! \n"
  CONTINUE_OR_CANCEL
  SEL_ROOT_PARTITION
 ;;
 "$EfiPart"|"$SwapPart")
  clear
  echo -e "\n \e[1;31m* $Error *\e[0m\n\n $PartNoTaken\n"
  CONTINUE_OR_CANCEL
  SEL_ROOT_PARTITION
 ;;
 esac
;;
*)
 clear
 SEL_ROOT_PARTITION
;;
esac
}

SEL_HOME_PARTITION () {
echo -e "\n\e[1;34m$Enter \e[1;36m$Number \e[1;34m$OfDiskPartition /home\e[0m\n\t( $Without \e[1;33m/dev/$Disk\e[0m, $ToSkip ):"
read HomePart
HomePart="${HomePart//'/dev/$Disk'/}" # Za svaki slučaj... :)
case "$HomePart" in
"")
 Home="Home: $Disk$RootPart ( $NoSepHomePart )"
;;
[1-9]|[1-9][0-9]|1[0-2][0-8])
 case "${Disk:0:4}" in
 "mmcb"|"nvme")
  HomePart="p$HomePart"
 ;;
 esac
 case "$HomePart" in
 "$EfiPart"|"$SwapPart"|"$RootPart")
  clear
  echo -e "\n \e[1;31m* $Error *\e[0m\n\n $PartNoTaken\n"
  CONTINUE_OR_CANCEL
  SEL_HOME_PARTITION
 ;;
 esac
 echo -e "\n\t$ToFormat /home ( /dev/\e[1;36m$Disk$HomePart\e[0m ) $Partition? ( $Yn )"
 read FormatHome
 FormatHome="${FormatHome,,}"
 Home="Home: $Disk$HomePart"
;;
*)
 clear
 SEL_HOME_PARTITION
;;
esac
case "$FormatHome" in # Potrebno za info prije početka instalacije
d*|y*|"")
 if [ "$HomePart" != "" ]; then
  WillFormatHome="$YesToExt\n\t\e[1;31m( * $Warning * $Disk$HomePart $WillBeErased! )"
 else
  WillFormatHome="\e[32m$NoSepHomePart\e[0m"
 fi
;;
*)
 WillFormatHome="$No"
;;
esac
}

HOST_NAME () {
clear
echo -e "\e[1;34m$EnterHostName\e[0m \e[36marchlinux\e[1;34m ):\e[0m"
read HostName
HostName="${HostName// /}" # Ukloni razmake
HostName="${HostName//'@'/AT}" # Zamjeni znak @
if [ "$HostName" = "" ]; then
 HostName="archlinux"
fi
}

function NET_DEVICE {
clear
echo -e "$CheckInternet..."
curl -s archlinux.org > /dev/null # Umjesto pinga jer ping ne radi u qemu virtualci
if [ $? != 0 ]; then
 echo -e "\n \e[1;31m* $Error *\e[0m\n\n $NoInternet $PleaseCheck.\n\n\e[1;34m $ConnectUsingWiFi?\e[0m ( $Yn )"
 read Connect
 Connect="${Connect,,}"
 case "$Connect" in
 ""|d*|y*)
  iwctl station wlan0 get-networks
  echo "$ToConnectUse"
  echo -e "\niwctl station connect SSID\n"
  CONTINUE_OR_CANCEL
 ;;
 n*)
  clear
  echo -e "\n \e[1;31m* $Error *\e[0m\n\n $NoInternet $PleaseCheck.\n"
  exit
 ;;
 esac
 sleep 2
 NET_DEVICE
else
 echo -e "\nInternet\t[ \e[1;32mOK\e[0m ]"
 sleep 1
 clear
fi
}

function TIMEZONE {
clear
i=0
ls -d /usr/share/zoneinfo/*/ | sed 's/\/usr\/share\/zoneinfo\///;s/\///' | grep -v "posix\|right" | while read RegionList
do
 i=$((i+1))
 if [ $i -lt 10 ]; then
  Num=" $i"
 else
  Num="$i"
 fi
 echo " $Num $RegionList" >> list
done
cat list
echo -e "\n\e[1;34m$Enter broj pored vremenske zone koju koristite:\e[0m"
read Region
Region=`grep " $Region " list`
rm list
Region="${Region// * }"
if [ "$Region" = "" ]; then
 echo -e "\n \e[1;31m* $Error *\e[0m\n\n Pogrešno upisan broj vremenske zone!\n"
 CONTINUE_OR_CANCEL
 TIMEZONE
fi
clear
i=0
ls /usr/share/zoneinfo/$Region/ | while read CityList
do
 i=$((i+1))
 if [ $i -lt 10 ]; then
  Num="  $i"
 elif [ $i -lt 100 ]; then
  Num=" $i"
 else
  Num="$i"
 fi
 echo " $Num $CityList" >> list
done
cat list | less --prompt "$Navigation"
echo -e "\n\e[1;34m$Enter točan broj pored grada:\e[0m"
read City
City=`grep " $City " list`
rm list
City="${City// * /}"
if [ "$City" = "" ]; then
 echo -e "\n \e[1;31m* $Error *\e[0m\n\n Pogrešno upisan broj grada!\n"
 CONTINUE_OR_CANCEL
 TIMEZONE
fi
echo -e "\n\e[1;34mVremenska zona koja će biti postavljena:\e[0m\n\n \e[1;36m$Region/$City\e[0m\n\n$ToContinue, $ToGoBack.\n"
read Status
case $Status in
b*|B*|n*|N*)
 TIMEZONE
;;
*)
 timedatectl set-ntp true
;;
esac
}

function PICK_DE {
clear
echo -e "\e[1;34m$EnterDE:\e[0m\n\n \e[36mN\e[0m = \e[36m$IllPickLater\e[0m <= default\n\n \e[36mC\e[0m = \e[36mCinnamon\n\n \e[36mG\e[0m = \e[36mGNOME\n\n K\e[0m = \e[36mKDE Plasma\n\n L\e[0m = \e[36mLXDE\n\n M\e[0m = \e[36mMATE\n\n X\e[0m = \e[36mXfce\n\e[0m"
read DEtoInstall
DEtoInstall="${DEtoInstall,,}"
case "$DEtoInstall" in
c*|g*|k*|l*|m*|x*)
 clear
 echo -e "\e[1;34m$AutoLoginAs \e[1;36m$UserName\e[1;34m?\e[0m ( $yN )"
 read AutoLogin
 AutoLogin="${AutoLogin,,}"
 echo -e "\n\e[1;34m$NumLockOn?\e[0m ( $Yn )"
 read NumLock
 NumLock="${NumLock,,}"
 case "$NumLock" in
 d*|y*|"")
  NumLock="d"
 ;;
 *)
  NumLock="n"
 ;;
 esac
;;
*)
 echo -e "\e[1;36m INFO:\e[31m $NoDEorWMinstall.\e[0m\n"
 NumLock="n"
 CONTINUE_OR_CANCEL
;;
esac
}

function INFO {
clear
if [ "$EfiPart" != "" ]; then
 echo -e "\e[1;34m$Overview:\e[0m\n\n $UserNameIs \e[36m$UserName\e[0m\n $HostNameIs:\t \e[36m$HostName\e[0m\n\n\e[1;34m$DiskPart:\e[0m\n"
 lsblk -o model,name,size,type,tran,mountpoint | grep "disk\|part" | grep $Disk
 echo -e "\n \e[36m$Efi\n $Swap\n Root: $Disk$RootPart\n $Home\e[0m\n\n\e[1;34m$FormPart:\e[0m
 \n EFI\t\e[36m$YesTo FAT32\e[0m\n /\t\e[36m$YesTo ext4\e[0m\n /home\t\e[36m$WillFormatHome\e[0m
 \n\e[1;34m$KeyLayout:\t\e[36m${Layout^^}\e[0m\n\n\e[1;34m$AllInfoIneed.\e[0m ;)\n"
 CONTINUE_OR_CANCEL
 echo -e " Formatiram $Disk$EfiPart...\n"
 umount /dev/$Disk$EfiPart 2>/dev/null
 mkfs.fat -F 32 /dev/$Disk$EfiPart
 if [ $? != 0 ]; then
  CONTINUE_OR_CANCEL
 fi
else
 echo -e "\e[1;34m$Overview:\e[0m\n\n $UserNameIs \e[36m$UserName\e[0m\n $HostNameIs:\t \e[36m$HostName\e[0m\n\n\e[1;34m$DiskPart:\e[0m\n"
 lsblk -o model,name,size,type,tran,mountpoint | grep "disk\|part" | grep $Disk
 echo -e "\n \e[36m$Swap\n Root: $Disk$RootPart\n $Home\e[0m\n\n\e[1;34m$FormPart:\e[0m
  \n swap \t\e[36m$YesIfSel )\e[0m\n /\t\e[36m$YesTo ext4\e[0m\n /home\t\e[36m$WillFormatHome\e[0m
 \n\e[1;34m$KeyLayout:\t\e[36m${Layout^^}\e[0m\n\n\e[1;34m$AllInfoIneed.\e[0m ;)\n"
 CONTINUE_OR_CANCEL
fi
}

function CONTINUE_OR_CANCEL {
echo -e "$ContinueOrCancel..."
read -p ""
}

function PASSWORD_EMPTY {
clear
echo -e "\n \e[1;31m* $Error *\e[0m\n\n $PassEmpty!\n"
CONTINUE_OR_CANCEL
}

function PASSWORD_MISMATCH {
clear
echo -e "\n \e[1;31m* $Error *\e[0m\n\n $PassMismatch!\n"
CONTINUE_OR_CANCEL
}

#==============================================================================#
clear
echo -e "\e[36m
 *******************************************************************************
\t$Welcome \e[1;36m$ArchLinux\e[0m\e[36m by \e[1;36mCooleech\e[0m\t\e[36m
 *******************************************************************************\e[0m
\e[1;36m
        A
       ARA
       RCRA  \e[0m$WhyThisScript! \e[1;36m
     ARCHCRA
    ARCH\e[0m\e[36mLH\e[0m\e[1;36mCRA  \e[1;31m* * * $Warning: $YourRisk * * *\e[0m\e[1;36m
   A\e[0m\e[36mRCHLILHCRA
  ARCHL   LHC    \e[0m$InfoGathering! \e[1;33m:)\e[0m\e[36m
 ARCHL     LHCRA
AR             RA
\e[0m"
echo -e "\n\e[1;34m$WhichKeyLayout?\e[0m\n\n \e[36mh\e[0m = \e[36m$Croatian\e[0m\t( CROAT ) <= default\n\n \e[36me\e[0m = \e[36m$English\e[0m\t( US )\n\n \e[36mo\e[0m = \e[36m$Other\e[0m\t( ?? )\n"
read UserKeyboard
case "$UserKeyboard" in
h*|c*|"")
 loadkeys croat # Postavi tipkovnicu na hrvatski layout
 Layout="croat"
;;
e*)
 loadkeys us # Postavi tipkovnicu na američki layout
 Layout="us"
;;
*)
 ENTER_KEYBOARD_LAYOUT
;;
esac
USER_NAME
ENTER_USER_PASS
ENTER_ROOT_PASS
PARTITIONING
HOST_NAME
NET_DEVICE
TIMEZONE
PICK_DE
INFO
if [ "$SwapPart" != "" ]; then
 echo -e " Formatiram $Disk$SwapPart...\n"
 swapoff /dev/$Disk$SwapPart 2>/dev/null
 mkswap /dev/$Disk$SwapPart
 if [ $? != 0 ]; then
  CONTINUE_OR_CANCEL
 fi
fi
echo -e " Formatiram $Disk$RootPart...\n"
umount /dev/$Disk$RootPart 2>/dev/null
mkfs.ext4 -Fq /dev/$Disk$RootPart
if [ $? != 0 ]; then
 CONTINUE_OR_CANCEL
fi
if [ "$HomePart" != "" ]; then
 case "$FormatHome" in
 d*|y*|"")
  echo -e " Formatiram $Disk$HomePart...\n"
  umount /dev/$Disk$HomePart 2>/dev/null
  mkfs.ext4 -Fq /dev/$Disk$HomePart
  if [ $? != 0 ]; then
   CONTINUE_OR_CANCEL
  fi
 ;;
 esac
fi
if [ "$SwapPart" != "" ]; then
 echo -e " Montiram swap particiju ($Disk$SwapPart)...\n"
 swapon /dev/$Disk$SwapPart
 if [ $? != 0 ]; then
  CONTINUE_OR_CANCEL
 fi
fi
echo -e " Montiram root particiju ($Disk$RootPart)...\n"
mount /dev/$Disk$RootPart /mnt
if [ $? != 0 ]; then
 CONTINUE_OR_CANCEL
fi
if [ "$EfiPart" != "" ]; then
 echo -e " Montiram EFI particiju ($Disk$EfiPart)...\n"
 mount --mkdir /dev/$Disk$EfiPart /mnt/boot/efi
 if [ $? != 0 ]; then
  CONTINUE_OR_CANCEL
 fi
fi
if [ "$HomePart" != "" ]; then
 echo -e " Montiram home particiju ($Disk$HomePart)...\n"
 mount --mkdir /dev/$Disk$HomePart /mnt/home
 if [ $? != 0 ]; then
  CONTINUE_OR_CANCEL
 fi
fi
clear
sed -i 's/#Color/Color/' /etc/pacman.conf
UUID=""
Arch=`uname -m`
case $Arch in
i?86)
 pacman -Sy --needed --noconfirm archlinux32-keyring archlinux-keyring
 pacstrap /mnt base linux linux-firmware nano sudo
;;
x86_64)
 pacman -Sy --needed --noconfirm archlinux-keyring
 if [ "$EfiPart" != "" ]; then
  EfiBootMgr=" efibootmgr"
 else
  EfiBootMgr=""
 fi
 pacstrap /mnt base base-devel$EfiBootMgr linux linux-firmware nano
 ls /sys/firmware/efi/efivars &> /dev/null
 if [ $? = 0 ]; then
  UUID=" -U"
 fi
;;
esac
if [ $? != 0 ]; then
 CONTINUE_OR_CANCEL
fi
echo -e "\n Generiranje fstab datoteke...\n"
genfstab$UUID /mnt | sed 's/rw,relatime/defaults,relatime/' >> /mnt/etc/fstab
#==============================================================================#
echo "#!/bin/bash
###############################
# What  : Arch_Linux_Chroot   #
# Which : version 2.28        #
# When  : 10.03.2024.         #
# Who   : Cooleech            #
# Where : GPLv2               #
# Write : cooleechATgmail.com #
###############################
function CONTINUE_OR_CANCEL {
 echo -e \"\n \e[1;31m* $Error *\e[0m\n\n$ContinueOrCancel...\"
 read -p \"\"
}
sed -i 's/PS1/#PS1/' /etc/skel/.bashrc
echo -e \"\n#Modificirani PS1\nclear\necho -e \\\"Arch Linux [Rolling Release]\\\\\n(\\\\\e[1;37mɔ\\\\\e[0m) \\\$(date +%Y) Arch Linux. All wrongs released.\\\\\n\\\"\nPS1='\\\$PWD> '\" >> /etc/skel/.bashrc
useradd -m -g users -G wheel,storage,power -c \"$FullName\" -s /bin/bash $UserName
echo -e \"$Pass1\n$Pass2\" | passwd $UserName # Postavi korisničku lozinku
echo -e \"$Pass3\n$Pass4\" | passwd # Postavi root lozinku
sed -i 's/#Color/Color/' /etc/pacman.conf
Arch=\`uname -m\`
case \$Arch in
i?86)
 pacman -Sy --noconfirm archlinux32-keyring
;;
x86_64)
 Line1=\`grep -n \"#\[multilib\]\" /etc/pacman.conf | sed 's/\:#\[multilib\]//'\` # Pronađi i izdvoji broj linije
 if [ \"\$Line1\" != \"\" ]; then
  Line2=\"\$(( \$Line1 + 1 ))\"
  sed -i \"\$Line1,\$Line2 s/#//\" /etc/pacman.conf # Odkomentiraj potrebne linije
  pacman -Sy --noconfirm lib32-alsa-plugins lib32-libpulse lib32-openal # Wine audio
 fi
;;
esac
sed -i 's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen
sed -i 's/#hr_HR.UTF/hr_HR.UTF/' /etc/locale.gen
locale-gen
echo -e \"LANG=en_US.UTF-8\nLC_MESSAGES=en_US.UTF-8\nLC_NUMERIC=hr_HR.UTF-8\nLC_TIME=hr_HR.UTF-8\nLC_MONETARY=hr_HR.UTF-8\nLC_PAPER=hr_HR.UTF-8\nLC_MEASUREMENT=hr_HR.UTF-8\" > /etc/locale.conf
export LANG=en_US.UTF-8
echo \"KEYMAP=$Layout\" > /etc/vconsole.conf # Tipkovnički raspored za konzolu
echo \"$HostName\" > /etc/hostname
echo -e \"127.0.0.1\tlocalhost\n::1\t\tlocalhost\n127.0.1.1\t${HostName}.localdomain\t$HostName\" >> /etc/hosts
pacman-db-upgrade # Fix za starije iso datoteke
sleep 1
pacman -S --needed --noconfirm alsa-firmware alsa-plugins alsa-utils amd-ucode bc dialog dnsmasq dosfstools grub intel-ucode lshw man-db mtools net-tools network-manager-applet ntp openssh os-prober perl-data-dump ufw usbutils util-linux wget xdg-user-dirs xorg-drivers xorg-server xorg-apps
if [ \$? != 0 ]; then
 CONTINUE_OR_CANCEL
fi
echo -e \"Section \\\"InputClass\\\"\n\tIdentifier \\\"system-keyboard\\\"\n\tMatchIsKeyboard \\\"on\\\"\n\tOption \\\"XkbLayout\\\" \\\"${Layout//croat/hr}\\\"\n\tOption \\\"XkbModel\\\" \\\"pc105\\\"\n\tOption \\\"XkbOptions\\\" \\\"terminate:ctrl_alt_bksp\\\"\nEndSection\" > /etc/X11/xorg.conf.d/20-keyboard.conf # Mora doći nakon instalacije xorg paketa
systemctl enable fstrim.timer
systemctl enable sshd
ln -sf /usr/share/zoneinfo/$Region/$City /etc/localtime
hwclock --systohc --utc
sed -i 's/pool.ntp.org/pool.ntp.org iburst/g' /etc/ntp.conf
sed -i 's/www.pool.ntp.org iburst/www.pool.ntp.org/g' /etc/ntp.conf # Fix za link
systemctl enable ufw
systemctl enable systemd-timesyncd
timedatectl set-ntp true
hwclock -w
echo -e \"polkit.addRule(function(action, subject) {\n\tif (action.id.indexOf(\\\"org.freedesktop.udisks2.\\\") == 0){\n\t\treturn polkit.Result.YES;\n\t}\n});\" > /usr/share/polkit-1/rules.d/10-drives.rules
AppPack=\"audacious cups epapirus-icon-theme firefox firefox-dark-reader firefox-ublock-origin ffmpeg freerdp gimp gstreamer gst-plugins-base gst-plugins-base-libs gst-plugins-good gst-plugins-ugly gvfs gvfs-mtp gvfs-smb libvncserver ntfs-3g p7zip system-config-printer ttf-dejavu ttf-droid unrar unzip wireless_tools wpa_supplicant xcursor-vanilla-dmz xterm vlc vorbis-tools zenity zip\"
case \"$DEtoInstall\" in
c*|g*|l*|m*|x*)
 pacman -S --needed --noconfirm gnome-keyring gnome-themes-standard
 if [ \$? != 0 ]; then
  CONTINUE_OR_CANCEL
 fi
 echo -e \"#!/bin/bash\n\nsource /etc/X11/xinit/xinitrc.d/30-dbus\neval \\\$(/usr/bin/gnome-keyring-daemon --start --components=pkcs11,secrets,ssh)\nexport SSH_AUTH_SOCK\" > /home/$UserName/.xinitrc
;;
esac
echo -e \"\nif [ -d /etc/X11/xinit/xinitrc.d ]; then\n for f in /etc/X11/xinit/xinitrc.d/*; do\n   [ -x \\\"\\\$f\\\" ] && . \\\"\\\$f\\\"\n done\n unset f\nfi\n\" >> /home/$UserName/.xinitrc
systemctl enable NetworkManager
systemctl enable NetworkManager-dispatcher.service
gpasswd -a $UserName network
sed -i 's/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/g' /etc/sudoers # ...and sudo for all
case \"$DEtoInstall\" in
c*)
 # Cinnamon
 pacman -S --needed --noconfirm cinnamon gnome-system-monitor gnome-terminal gpicview lightdm lightdm-slick-greeter mousepad nemo-image-converter nemo-fileroller nemo-preview numlockx papirus-icon-theme
 if [ \$? != 0 ]; then
  CONTINUE_OR_CANCEL
 fi
 systemctl enable lightdm.service
;;
g*)
 # GNOME
 pacman -S --needed --noconfirm gnome gnome-extra
 if [ \$? != 0 ]; then
  CONTINUE_OR_CANCEL
 fi
 systemctl enable gdm.service
;;
k*)
 # KDE
 pacman -S --needed --noconfirm ark dolphin ffmpegthumbs gwenview kate kcalc konsole krdc packagekit-qt5 plasma-meta spectacle
 if [ \$? != 0 ]; then
  CONTINUE_OR_CANCEL
 fi
 mkdir /home/$UserName/Desktop
 mkdir /home/$UserName/.conf
# echo -e \"[Desktop Entry]\nCategories=Qt;KDE;System;FileTools;FileManager;\nExec=dolphin %u\nGenericName=File Manager\nIcon=user-home\nInitialPreference=10\nKeywords=files;file management;file browsing;samba;network shares;Explorer;Finder;\nMimeType=inode/directory;\nName=Home\nStartupNotify=true\nStartupWMClass=dolphin\nTerminal=false\nType=Application\nX-DBUS-ServiceName=org.kde.dolphin\nX-DocPath=dolphin/index.html\nX-KDE-Shortcuts=Meta+E\nX-KDE-SubstituteUID=false\" > /home/$UserName/Desktop/home.desktop
 echo -e \"[Desktop Entry]\nName=Trash\nComment=Contains removed files\nIcon=user-trash-full\nEmptyIcon=user-trash\nType=Link\nURL=trash:/\nOnlyShowIn=KDE;\" > /home/$UserName/Desktop/trash.desktop
 chmod -R 755 $UserName /home/$UserName/Desktop/*.desktop
# echo -e \"[Formats]\nLANG=en_US.UTF-8\nLC_ADDRESS=hr_HR.UTF-8\nLC_MEASUREMENT=hr_HR.UTF-8\nLC_MONETARY=hr_HR.UTF-8\nLC_NAME=hr_HR.UTF-8\nLC_NUMERIC=hr_HR.UTF-8\nLC_PAPER=hr_HR.UTF-8\nLC_TELEPHONE=hr_HR.UTF-8\nLC_TIME=hr_HR.UTF-8\" > /home/$UserName/.conf/plasma-localerc
 systemctl enable sddm.service
 touch /etc/sddm.conf
;;
l*)
 # LXDE
 pacman -S --needed --noconfirm galculator gnome-mplayer leafpad lxde lxdm obconf xarchiver
 if [ \$? != 0 ]; then
  CONTINUE_OR_CANCEL
 fi
 systemctl enable lxdm.service
;;
m*)
 # MATE
 pacman -S --needed --noconfirm gtk-engine-murrine lightdm lightdm-slick-greeter mate mate-extra numlockx
 if [ \$? != 0 ]; then
  CONTINUE_OR_CANCEL
 fi
 systemctl enable lightdm.service
;;
x*)
 # XFCE
 pacman -S --needed --noconfirm lightdm lightdm-gtk-greeter pavucontrol thunar-archive-plugin thunar-volman xarchiver xfce4 xfce4-goodies xfce4-notifyd numlockx
 if [ \$? != 0 ]; then
  CONTINUE_OR_CANCEL
 fi
 systemctl enable lightdm.service
 echo -e \"style \\\"xfdesktop-icon-view\\\" {
	XfdesktopIconView::label-alpha = 0\n\tXfdesktopIconView::shadow-x-offset = 1\n\tXfdesktopIconView::shadow-y-offset = 1\n\tXfdesktopIconView::shadow-color = \\\"#000000\\\"\n\tXfdesktopIconView::selected-shadow-x-offset = 0\n\tXfdesktopIconView::selected-shadow-y-offset = 0
	XfdesktopIconView::selected-shadow-color = \\\"#000000\\\"\n\tfg[NORMAL] = \\\"#ffffff\\\"\n\tfg[SELECTED] = \\\"#ffffff\\\"\n\tfg[ACTIVE] = \\\"#ffffff\\\" }\n\twidget_class \\\"*XfdesktopIconView*\\\" style \\\"xfdesktop-icon-view\\\"\" >> /home/$UserName/.gtkrc-2.0
;;
*)
 echo -e \" \e[1;36mINFO:\e[31m $NoDEorWMinstall! \e[0m\n\"
;;
esac
xdg-user-dirs-update --force
chown -R $UserName /home/$UserName
if [ -e /etc/lightdm/lightdm.conf ]; then
 which lightdm-gtk-greeter
 if [ \$? != 0 ]; then
  sed -i \"s/#greeter-session=example-gtk-gnome/greeter-session=lightdm-slick-greeter/\" /etc/lightdm/lightdm.conf
 fi
fi
case \"$DEtoInstall\" in
c*|g*|k*|l*|m*|x*)
 pacman -S --needed --noconfirm \$AppPack
 if [ \$? != 0 ]; then
  echo -e \"*** Error installing app pack ***\n\nYou can do it manually later\nwhen OS is up and running.\n\"
  echo -e \"#!/bin/bash\nsudo pacman -S --needed --noconfirm \$AppPack\" > /home/$UserName/Install_missing_apps.sh
  chmod 777 /home/$UserName/Install_missing_apps.sh
  CONTINUE_OR_CANCEL
 fi
;;
*)
 echo -e \" All apps installed correctly!\n\"
;;
esac
# TODO: GDM NUMLOCK
case \"$NumLock\" in
d)
 if [ -e /etc/lightdm/lightdm.conf ]; then
  sed -i \"s|#greeter-setup-script=|greeter-setup-script=/usr/bin/numlockx on|\" /etc/lightdm/lightdm.conf
 fi
 if [ -e /etc/lxdm/lxdm.conf ]; then
  sed -i 's/# numlock=0/numlock=1/' /etc/lxdm/lxdm.conf # bash alternative: setleds -D +num >> ~/.bash_profile
 fi
 if [ -e /etc/sddm.conf ]; then
  echo -e \"[General]\nNumlock=on\n\n[Theme]\nCurrent=breeze\nCursorTheme=Breeze_Snow\n\" > /etc/sddm.conf
 fi
;;
n)
 if [ -e /etc/sddm.conf ]; then
  echo -e \"[Theme]\nCurrent=breeze\nCursorTheme=Breeze_Snow\n\" > /etc/sddm.conf
 fi
;;
esac
echo -e \"alias la='ls -a'\nalias ll='ls -la'\nalias grep='grep --color=auto'\" >> /etc/bash.bashrc
case \"$AutoLogin\" in
d*|y*)
 if [ -e /etc/gdm/custom.conf ]; then
  sed -i \"/daemon/ a\AutomaticLogin=$UserName\nAutomaticLoginEnable=True\" /etc/gdm/custom.conf # Thnx, vision! :)
 fi
 if [ -e /etc/lightdm/lightdm.conf ]; then
  groupadd -r autologin
  gpasswd -a $UserName autologin
  sed -i \"s/#autologin-user=/autologin-user=$UserName/\" /etc/lightdm/lightdm.conf
 fi
 if [ -e /etc/lxdm/lxdm.conf ]; then
  sed -i \"s/# autologin=dgod/autologin=$UserName/\" /etc/lxdm/lxdm.conf
 fi
 if [ -e /etc/sddm.conf ]; then
  echo -e \"[Autologin]\nUser=$UserName\nSession=plasma.desktop\" >> /etc/sddm.conf
 fi
;;
esac
if [ \"$EfiPart\" != \"\" ]; then
 echo -e \"\n EFI instalacija GRUB-a:\"
 grub-install --force --removable --target=x86_64-efi --efi-directory=/boot/efi
 echo -e \"\n MBR instalacija GRUB-a:\"
 grub-install --force --target=i386-pc /dev/$Disk
else
 grub-install --force --target=i386-pc /dev/$Disk
fi
if [ \$? != 0 ]; then
 CONTINUE_OR_CANCEL
fi
grub-mkconfig -o /boot/grub/grub.cfg
if [ \$? != 0 ]; then
 CONTINUE_OR_CANCEL
else
 sed -i \"s/Linux linux/Linux kernel/g\" /boot/grub/grub.cfg
fi
rm -f /root/.bashrc
rm -f /etc/ArchChroot" > /mnt/etc/ArchChroot
#==================================================================================================#
echo -e "sh /etc/ArchChroot\nexit" > /mnt/root/.bashrc
arch-chroot /mnt /bin/bash
clear
umount -R /mnt
swapoff -a
sleep 5 | echo -e "\n\e[36m*********************************\n*\t\e[37m$InstallEnd\e[36m\t*\n*********************************\e[0m\n\n $EnjoyWith \e[1;36m$ArchLinux \e[1;33m:)\e[0m\n $EnterToReboot\n\n $RebootIn"
reboot